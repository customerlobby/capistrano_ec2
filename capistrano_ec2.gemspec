# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'capistrano_ec2/version'

Gem::Specification.new do |spec|
  spec.name          = "capistrano_ec2"
  spec.version       = CapistranoEc2::VERSION
  spec.authors       = ["Rob Hunt"]
  spec.email         = ["rhunt@customerlobby.com"]
  spec.summary       = "use aws API to detect ec2 instances for your environemnt"
  spec.description   = "use aws API to detect ec2 instances for your environemnt"
  spec.homepage      = "http://www.customerlobby.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency 'rspec'

  spec.add_runtime_dependency 'aws-sdk'
end
