# CapistranoEc2

A gem which uses the AWS API to read available ec2 instances by environment

## Installation

    
Add this line to your application's Gemfile:

```ruby
gem 'capistrano_ec2'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install capistrano_ec2

## Usage

    ec2 = CapistranoEc2::Detector.new('<your access_key_id>','<your secret_access_key>')
    ec2.get_hosts_for_environment("production") retuns a hash with the instance roles as keys, and an array of public dns names as values

## Contributing

1. Fork it ( https://bitbucket.org/customerlobby/capistrano_ec2/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
