require "capistrano_ec2/version"
require 'aws-sdk'

module CapistranoEc2
  class Detector
    def initialize(access_key_id=nil, secret_access_key=nil)
      if access_key_id.nil? || secret_access_key.nil?
        raise Required
      end
      @ec2 = AWS::EC2.new(:access_key_id => access_key_id,:secret_access_key => secret_access_key)
    end

    def get_instances(environment='staging')
      return @ec2.instances.select {|instance| instance.tags.to_h["Environment"] == environment}
    end

    def filter_instances_by_role(role)
      get_instances.select {|instance| instance.tags.to_h['Role'] == role}
    end

    def get_hosts_for_environment(environment='staging')
      result = {}
      get_instances(environment).each do |instance|
        if !result.has_key?(instance.tags.to_h['Role'])
          result[instance.tags.to_h['Role']] = [instance.public_dns_name]
        else
          result[instance.tags.to_h['Role']].push(instance.public_dns_name)
        end
      end
      return result
    end
  end
end
