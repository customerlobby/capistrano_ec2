require 'spec_helper'



describe 'CapistranoEc2::Detector' do
  it 'should pretend to connect to aws and read a fake list of instances for an environment' do
    ec2 = CapistranoEc2::FakeDetector.new('MYACCESSKEY','MySecretAccessKeyDontTellAnyone')
    instances = ec2.get_hosts_for_environment('production')
    expect(instances.keys.count).to eq(5)
    expect(instances.keys).to include('worker00')
    expect(instances['worker00'].count).to be > 0
    expect(instances.keys).to include('worker01')
    expect(instances['worker01'].count).to be > 0
    expect(instances.keys).to include('database')
    expect(instances['database'].count).to be > 0
    expect(instances.keys).to include('services')
    expect(instances['services'].count).to be > 0
    expect(instances.keys).to include('app')
    expect(instances['app'].count).to eq(2)
  end
end
