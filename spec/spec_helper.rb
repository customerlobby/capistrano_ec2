require 'capistrano_ec2'

# Fake just enough of what aws _should_ send us so that we can test the get_hosts_for_environment method without
# having to actually talk to aws during testing.  Note: this means that if amazon ever changes the format of what they
# send us, our tests will not detect it and will continue to pass (but our deploy scripts will fail)
module CapistranoEc2
  class FakeDetector < Detector
    def get_instances(environment='staging')
      result = []
      result.push(AWS::EC2::Instance.new(1, {'Environment' => environment, 'Role' => 'worker00'},  'fakeEc2-127-0-0-1.compute-1-.fakeamazonaws.com'))
      result.push(AWS::EC2::Instance.new(2, {'Environment' => environment, 'Role' => 'worker01'},  'fakeEc2-127-0-0-2.compute-1-.fakeamazonaws.com'))
      result.push(AWS::EC2::Instance.new(3, {'Environment' => environment, 'Role' => 'database'},  'fakeEc2-127-0-0-3.compute-1-.fakeamazonaws.com'))
      result.push(AWS::EC2::Instance.new(4, {'Environment' => environment, 'Role' => 'app'},       'fakeEc2-127-0-0-4.compute-1-.fakeamazonaws.com'))
      result.push(AWS::EC2::Instance.new(5, {'Environment' => environment, 'Role' => 'app'},       'fakeEc2-127-0-0-5.compute-1-.fakeamazonaws.com'))
      result.push(AWS::EC2::Instance.new(6, {'Environment' => environment, 'Role' => 'services'},  'fakeEc2-127-0-0-6.compute-1-.fakeamazonaws.com'))
      return result
    end
  end
end

# -- simulate just enough of a AWS::EC2::Instance object to test the get_hosts_for_environment method with the data faked above
module AWS
  class EC2
    class Instance
      attr_reader :tags, :instance_id
      def initialize(instance_id, tags, dns)
        @instance_id = instance_id
        @tags = tags
        @dns = dns
      end
      def public_dns_name
        return @dns
      end
    end
  end
end